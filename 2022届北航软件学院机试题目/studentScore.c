//
//  studentScore.c
//  学生成绩排序
//
//  Created by zhuoooo on 2023/3/24.
//  Copyright © 2022年 zhuoooo. All rights reserved.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#define N 101
struct student{
    int id;
    char name[21];
    double score;
}stu[N];


int main(){
    int i, j, k; 
    int num;
    struct student temp;

    scanf("%d", &num);
    for (i = 0; i < num; i++) {
        scanf("%d %s %lf", &stu[i].id, &stu[i].name, &stu[i].score);
    }

    for (i = 0; i < num-1; i++) {
        k = i;
        for (j = i+1; j < num; j++) {
            if (stu[j].score > stu[k].score) {
                k = j;
            }else if(stu[j].score == stu[k].score) {
                if (strcmp(stu[j].name,  stu[k].name) < 0) {
                    k = j;
                }else if (strcmp(stu[j].name,  stu[k].name) == 0) {
                    if (stu[j].id < stu[k].id) {
                        k = j;
                    }
                }
            }
        }
        temp = stu[k];
        stu[k] = stu[i];
        stu[i] = temp;
    }

    for (i = 0; i < num; i++) {
        printf("%d %d %s %.2lf\n", i+1, stu[i].id, stu[i].name, stu[i].score);
    }

}