//
//  mathCalc.c
//  时间简易整数计算器
//
//  Created by zhuoooo on 2023/3/22.
//  Copyright © 2022年 zhuoooo. All rights reserved.

#include <stdio.h>
#include <stdlib.h>

int main(){
    int op;
    long num1, num2;
    long res;

    scanf("%d %ld %ld", &op, &num1, &num2);

    switch (op) {
        case 1 ://加法
            res = num1 + num2;
            break;    
        case 2://减法
            res = num1 - num2;
            break;
        case 3://乘法
            res = num1 * num2;
            break;
        case 4://除法
            if (num2 == 0) {
                printf("DIV0");
                return 0;
            }else {
                res = num1 / num2;
            }
            break;
    }

    printf("%ld", res);

    return 0;
}