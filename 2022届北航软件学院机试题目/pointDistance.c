//
//  pointDistance.c
//  点到集合的距离
//
//  Created by zhuoooo on 2023/3/22.
//  Copyright © 2022年 zhuoooo. All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#define N 2001

int main(){
    int i, n;
    int y[N];
    int x, res=3000;

    scanf("%d", &n);
    for (i = 0; i < n; i++) {
        scanf("%d", &y[i]);
    }
    scanf("%d", &x);

    int dis[n];
    for (i = 0; i < n; i++) {
        if (x > y[i]) 
            dis[i] = x - y[i];
        else
            dis[i] = y[i] - x;

        if (res > dis[i]) {
            res = dis[i];
        } 
    }
    printf("%d", res);

}