//
//  charTransform.c
//  字符转换
//
//  Created by zhuoooo on 2023/3/22.
//  Copyright © 2022年 zhuoooo. All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#define N 10000

int main(){
    int i, n;
    char str[N];
    int carry[256];
    int res[N];

    scanf("%s", str);

    for (i = 0; i < 10; i++) {
        carry['0'+i] = i;
    }
    for (i = 0; i < 26; i++) {
        carry['A'+i] = 10+i;
    }
    for (i = 0; i < 26; i++) {
        carry['a'+i] = -11-i;
    }
    carry['!'] = -1;
    carry['@'] = -2;
    carry['#'] = -3;
    carry['$'] = -4;
    carry['%'] = -5;
    carry['?'] = -6;
    carry['&'] = -7;
    carry['*'] = -8;
    carry['('] = -9;
    carry[')'] = -10;

    n = strlen(str);
    printf("%d\n", n);
    
    for (i = 0; i < n; i++) {
        res[i] = carry[str[i]];
        printf("%d ", res[i]);
    }  
    printf("\n");  


    return 0;
}