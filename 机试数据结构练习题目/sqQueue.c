//
//  main.c
//  main4
//
//  Created by zhuoooo on 2022/11/11.
//  Copyright © 2022年 zhuoooo. All rights reserved.
//  队列的操作实现

#include <stdio.h>
#include <stdlib.h>
#define OK 1
#define ERROR -1
#define MAX_SIZE 10
typedef int ElemType;
typedef int Status;
typedef struct Queue
{
    ElemType Queue_array[MAX_SIZE];
    int front;
    int rear;
}SqQueue;

Status InitQueue(SqQueue *Q);//初始化队列
Status EnQueue(SqQueue *Q, ElemType e);//入队列  插入元素e为队列的队尾元素
Status DeQueue(SqQueue *Q);//若队列不为空，则删除Q的队头元素，并返回OK；否则返回ERROR；
int QueueLength(SqQueue Q);//返回Q的元素个数，其队列的长度
void OutPut(SqQueue *Q);//打印输出队列

int main(int argc, const char * argv[]) {
    // insert code here...
    
    int res = 0;
    int num;
    SqQueue Q;

    printf("1.初始化队列\n2.入队列\n3.输出队列\n4.遍历队列\n5.删除队首元素\n-1.退出\n");
    while (1) {
        printf("请输入编号:\n");
        scanf("%d", &num);
        
        if (num == 1) {
            res = InitQueue(&Q);
            if (res == 1) {
                printf("队列初始化成功\n");
            }else{
                printf("队列初始化失败\n");
            }
        }
        else if (num == 2){
            int e;
            
            printf("入队元素：");
            scanf("%d", &e);
            res = EnQueue(&Q, e);
            
            if (res == 1) {
                printf("插入队列成功\n");
            }else{
                printf("插入队列失败\n");
            }
        }
        else if (num == 3){
            OutPut(&Q);
        }
        else if (num == 4){
            res = QueueLength(Q);
            printf("输出队列的长度：%d\n", res);
        }
        else if (num == 5){
            res = DeQueue(&Q);
            if (res == 1) {
                printf("队列删除成功\n");
            }else{
                printf("队列删除失败\n");
            }
        }
        else if(num == -1){
            printf("退出\n");
            break;
        }
        else{
            printf("输入错误，请重新输入\n");
        }
    }
    return 0;
}
Status InitQueue(SqQueue *Q)//初始化队列
{
    Q->front = 0;
    Q->front = 0;
    return OK;
}
Status EnQueue(SqQueue *Q, ElemType e)//入队列  插入元素e为队列的队尾元素
{
    if ((Q->rear + 1) % MAX_SIZE == Q->front) {//队列满
        printf("队列已满\n");
        return ERROR;
    }else{
        Q->Queue_array[Q->rear] = e;
        Q->rear = (Q->rear + 1) % MAX_SIZE;
        return OK;
    }
}
Status DeQueue(SqQueue *Q)//若队列不为空，则删除Q的队头元素，并返回OK；否则返回ERROR；
{
    if (Q->front == Q->rear) {
        return ERROR;
    }else{
        Q->front = (Q->front + 1) % MAX_SIZE;
        return OK;
    }
}
int QueueLength(SqQueue Q)//返回Q的元素个数，其队列的长度
{
    return (Q.rear - Q.front + MAX_SIZE) % MAX_SIZE;
}
void OutPut(SqQueue *Q)//打印输出队列
{
    int count = 1;
    int p;
    
    p = Q->front;
    if (Q->front == Q->rear) {
        printf("队列为空\n");
    }else{
        while (p % MAX_SIZE != Q->rear) {
            printf("第%d个元素是:%d\n", count, Q->Queue_array[p]);
            p = (p + 1) % MAX_SIZE;
            count++;
        }
    }
}











