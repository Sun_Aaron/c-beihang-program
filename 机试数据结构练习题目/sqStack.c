//
//  main.c
//  main3
//
//  Created by zhuoooo on 2022/11/7.
//  Copyright © 2022年 zhuoooo. All rights reserved.
//  栈的实现操作

#include <stdio.h>
#include <stdlib.h>
#define STACK_SIZE 10
#define OK 1
#define ERROR -1
#define STACKINCREMENT 10/*存储空间分配增量*/
typedef int ElemType;
typedef int Status;
typedef struct SqStack
{
    int top;
    int bottom;
    ElemType array[STACK_SIZE];
}SqStack;

Status InIt_SqStack(SqStack *L);//初始化栈
Status Push(SqStack *L, ElemType e);//入栈  e为待压入的元素
Status Pop(SqStack *L);//出栈
Status StackLength(SqStack *L);//遍历栈
Status ClearStack(SqStack *L);//清空栈
void OutPut_Stack(SqStack *L);//输出栈


int main(int argc, const char * argv[]) {
    
    SqStack L;
    int res = 0;
    int i, count, len;
    
    //----------------------初始化栈--------------------------
    
    res = InIt_SqStack(&L);
    if (res == 1) {
        printf("初始化栈成功\n");
    }else
    {
        printf("初始化栈失败\n");
    }
    
    //----------------------入栈--------------------------
    res = 0;
    printf("请输入入栈的个数和元素:\n");
    scanf("%d", &len);
    
    for (i = 0; i < len; i++) {
        scanf("%d", &count);
        res = Push(&L, count);
    }
    if (res == 1) {
        printf("入栈成功\n");
    }else
    {
        printf("入栈失败");
    }
    
    //----------------------获取栈的长度--------------------------
    printf("获取栈的长度\n");
    res = StackLength(&L);
    printf("栈的长度:%d\n", res);
    
    OutPut_Stack(&L);//输出元素
    
    //----------------------出栈--------------------------
    
    res = 0;
    printf("弹出栈顶元素");
    res = Pop(&L);
    if(res != -1)
    {
        printf("栈顶元素弹出成功\n");
        printf("栈顶元素为：%d\n", res);
    }
    
    
    
    return 0;
}

Status InIt_SqStack(SqStack *L)//初始化栈
{
    L->top = 0;
    L->bottom = 0;
    
    return OK;
}

Status Push(SqStack *L, ElemType e)//入栈  e为待压入的元素
{
    int res = 0;
    if(L->top - L->bottom == STACK_SIZE - 1)
    {
        printf("栈满\n");
        res = ERROR;
    }
    else{
        L->array[L->top] = e;
        L->top++;
        res = OK;
    }
    return res;
}

Status Pop(SqStack *L)//出栈
{
    int e = -1;
    if (L->top == L->bottom) {
        printf("栈为空\n");
    }
    e = L->array[L->top - 1];
    L->top--;
    return e;
}

Status GetTop(SqStack *L)//获取栈顶元素
{
    int e;
    if (L->top == L->bottom) {
        printf("栈为空\n");
    }
    else{
        L->top--;
        e = L->array[L->top];
        L->top++;
        L->array[L->top] = e;
    }
    
    return e;
}

Status StackLength(SqStack *L)//遍历栈
{
    int i;
    int length = 0;
    
    if (L->top == L->bottom) {
        printf("栈为空\n");
    }
    else{
        for (i = 0; i < L->top; i++) {
            length++;
        }
    }
    return length;
}


Status ClearStack(SqStack *L)//清空栈
{
    L->top = L->bottom;
    return OK;
}

void OutPut_Stack(SqStack *L)//输出栈
{
    int i;
    int length = 0;
    
    if (L->top == L->bottom) {
        printf("栈为空\n");
    }
    for (i = 0; i < L->top; i++) {
        length++;
    }
    
    int len = length;
    for (i = len - 1; i >= 0; i--) {
        printf("第%d个元素是:%d\n", len, L->array[i]);
        len--;
    }
}






