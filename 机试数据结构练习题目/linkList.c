//
//  main.c
//  main2
//
//  Created by zhuoooo on 2022/11/2.
//  Copyright © 2022年 zhuoooo. All rights reserved.
//  线性表的实现

#include <stdio.h>
#include <stdlib.h>
#define OK 1
#define ERROR -1

typedef int Status;
typedef int ElemType;
typedef struct LNode{
    ElemType data;
    struct LNode *next;
}LNode;

Status create_LinkList(LNode *L);//创建线性表;
Status ListInsert(LNode *L, int i, ElemType e);//在线性表的i位置处插入元素e;
Status Query(LNode *L, int i, ElemType count);//查找元素位置
Status Delete_LNode(LNode *L, int i, int count);//删除元素
Status Travel(LNode *L);//返回线性表个数
void DisplayLNode(LNode *L, ElemType count); //显示线性表的数据;

int main(int argc, const char * argv[]) {
    // insert code here...
    
    LNode L;
    int count, res = 0;
    int i, j, k, e, num;
    
    printf("菜单指令：\n");
    printf("1.生成线性表\n2.插入元素\n3.查找元素\n4.删除元素\n5.输出元素\n6.统计元素个数\n7.请重新输入\n-1.退出");
    while (1) {
        printf("请输入操作编号：\n");
        scanf("%d", &num);
        
        if (num == 1) {
            //---------------------------生成线性表-------------------------
            printf("请输入线性表的长度：");
            scanf("%d", &count);
            res = create_LinkList(&L);
            if(res == 1)
                printf("线性表L初始化成功\n");
        }
        else if (num == 2){
            //---------------------------插入元素-------------------------
            res = 0;
            printf("请输入你要插入的元素的数值:\n");
            for(i = 0; i < count; i++)
            {
                scanf("%d", &e);
                res = ListInsert(&L, i+1, e);
            }
            if(res == 1)
                printf("线性表数据添加成功\n");
        }
        else if (num == 3){
            //--------------------------查找元素-------------------------
            res = 0;
            printf("请输入要查找元素的位置\n");
            scanf("%d", &k);
            res = Query(&L, k, count);
            if(res == 1)
                printf("线性表数据查找成功\n");
        }
        else if (num == 4){
            //--------------------------删除元素-------------------------
            res = 0;
            printf("请输入要删除元素的位置\n");
            scanf("%d", &j);
            res = Delete_LNode(&L, j, count);
            if(res == 1)
                printf("线性表数据删除成功\n");
        }
        else if (num == 5){
            //--------------------------输出元素-------------------------
            DisplayLNode(&L, count);
            
        }
        else if (num == 6){
            //--------------------------统计元素个数-------------------------
            count = Travel(&L);
            printf("元素还剩余%d\n", count);
        }
        else if (num == -1){
            printf("退出");
            break;
        }
        else
            printf("请重新输入\n");
    }
    
    return 0;
}



Status create_LinkList(LNode *L)//创建线性表
{
    L->data = 0;
    L->next = NULL;
    
    if(L->next == NULL)
        return OK;
    else
        return ERROR;
}

Status ListInsert(LNode *L, int i, ElemType e)//在线性表的i位置处插入元素e;
{
    int j = 0;
    LNode *s;
    LNode *p = L;
    
    while(p && j < i-1)
    {
        p = p->next;
        j++;
    }
    if(!p || j > i-1)
        return ERROR;
    else{
        s = (LNode *) malloc (sizeof(LNode));//生成新节点
        s->data = e;
        s->next = p->next;
        p->next = s;
    }
    return OK;
}

Status Query(LNode *L, int i, ElemType count)//查找元素位置
{
    LNode *p = L;
    int j;
    
    if (i <= 0 || i > count) {
        printf("查找元素不存在\n");
        return ERROR;
    }else{
        for (j = 0; j < i; j++) {
            p = p->next;
        }
        printf("The No.%d number is :%d\n", j, p->data);
    }
    return OK;
}

Status Delete_LNode(LNode *L, int i, int count)//删除元素
{
    LNode *p, *q;
    int j;
    p = L;
    
    if (i <= 0 || i > count) {
        printf("您输入元素的位置不存在\n");
        return ERROR;
    }else{
        for (j = 0; j < i; j++) {
            q = p;
            p = p->next;
        }
        q->next = p->next;
        p = q;
        
        return OK;
    }
}

Status Travel(LNode *L)//返回线性表个数
{
    int count = 0;
    LNode *p = L;
    
    while(p->next != NULL)
    {
        count++;
        p = p->next;
    }
    return count;
    
}

void DisplayLNode(LNode *L, ElemType count)//显示线性表的数据;
{
    LNode *p;
    int i = count - 1;
    
    p = L->next;
    while(p != NULL)
    {
        printf("The No.%d number is :%d\n", count-i ,p->data);
        i--;
        p = p->next;
    }
}
