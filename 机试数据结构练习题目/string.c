//
//  main.c
//  String
//
//  Created by zhuoooo on 2022/11/16.
//  Copyright © 2022年 zhuoooo. All rights reserved.
//  字符串匹配

#include <stdio.h>
#include <stdlib.h>
#define OK 1
#define ERROR -1
#define MAX_SIZE 100
typedef int Status;
typedef struct {
    char ch[MAX_SIZE];
    int length;
}String;

Status Init_String(String *S);//初始化字符串
Status StrAssign(String *S, char chars[]);//生成一个其值等于chars的字符串
void OutPutString(String *S);//输出字符串
Status CreateString(String *S);//创建一个字符串
Status StrCompare(String *S1, String *S2);//比较字符串的大小
Status Concat(String *S1, String *S2, String *S);//连接2个字符串
void StringLength(String *S);//求字符串的长度
Status SubString(String *S, String *Sub, int pos, int len);//求字符串的子串
Status Index(String *S, int pos, String *T);//字符串的模式匹配

int main(int argc, const char * argv[]) {
    // insert code here...
    int res = 0;
    String S, T, R;
    String sub;
    int num;
    
    printf("1.初始化一个字符串\n2.生成一个其值等于输入chars的字符串\n3.创建一个字符串\n4.比较字符串\n5.连接2个字符串\n6.求字符串的长度\n7.求字符串的子串\n8.输出字符串\n9.模式匹配\n10.退出\n");
    while (1) {
        printf("请输入您要执行的编号：");
        scanf("%d", &num);
        
        if (num == 1) {
            res = Init_String(&S);
            if (res == 1) {
                printf("初始化字符串成功！\n");
            }else{
                printf("初始化字符串失败！\n");
            }
        }else if (num == 2){
            char chars[MAX_SIZE];
            printf("输入一个chars的字符串：");
            scanf("%s", chars);
            res = StrAssign(&S, chars);
            if (res == 1) {
                printf("字符串S复制成功,S=");
                OutPutString(&S);
            }else{
                printf("");
            }
        }else if (num == 3){
            res = CreateString(&T);
            if (res == 1) {
                printf("创建字符串成功T=");
                OutPutString(&T);
            }else{
                printf("创建字符串失败！\n");
            }
        }else if (num == 4){
            res = StrCompare(&S, &T);
            printf("两个字符串的差值是：%d\n", res);
        }else if (num == 5){
            res = Concat(&S, &T, &R);
            if (res == 1) {
                printf("字符串连接成功R=");
                OutPutString(&R);
            }else{
                printf("字符串连接失败！");
            }
        }else if (num == 6){
            printf("字符串S的长度是：");
            StringLength(&S);
            printf("字符串T的长度是：");
            StringLength(&T);
            printf("字符串R的长度是：");
            StringLength(&R);
        }else if (num == 7){
            int pos, len;
            printf("请输入R的子串的起始位置和长度：");
            scanf("%d", &pos);
            scanf("%d", &len);
            res = SubString(&R, &sub, pos, len);
        }else if (num == 8){
            printf("字符串S=");
            OutPutString(&S);
            printf("字符串T=");
            OutPutString(&T);
            printf("字符串R=");
            OutPutString(&R);
        }else if (num == 9){
            int b;
            printf("请输入元素匹配的一个位置:\n");
            scanf("%d", &b);
            
            res = Index(&S, b, &T);
        }else if (num == 10){
            printf("退出成功！");
            break;
        }else{
            continue;
        }
    }
    return 0;
}

Status Init_String(String *S)//初始化字符串
{
    S->length = 0;
    return OK;
}
Status StrAssign(String *S, char chars[])//生成一个其值等于chars的字符串
{
    int i;

    for (i = 0; chars[i] != '\0'; i++) {
        S->ch[i] = chars[i];
        S->length++;
    }
    return OK;
}

void OutPutString(String *S)//输出字符串
{
    int i;
    
    for (i = 0; i < S->length; i++) {
        printf("%c", S->ch[i]);
    }
    printf("\n");
}

Status CreateString(String *S)//创建一个字符串
{
    int i;
    char chars[MAX_SIZE];
    
    S->length = 0;
    printf("请输入一个字符串：");
    scanf("%s", chars);
    
    for (i = 0; chars[i] != '\0'; i++) {
        S->ch[i] = chars[i];
        S->length++;
    }
    return OK;
}
Status StrCompare(String *S1, String *S2)//比较字符串的大小
{
    int i;
    
    for (i = 0; i < S1->length && i < S2->length; i++) {
        if (S1->ch[i] != S2->ch[i]) {
            return S1->ch[i] - S2->ch[i];
        }
    }
    return S1->length - S2->length;
}
Status Concat(String *S1, String *S2, String *S)//连接2个字符串
{
    int i, j;
    
    for (i = 0; i < S1->length; i++) {
        S->ch[i] = S1->ch[i];
        S->length++;
    }
    
    for (j = 0; j < S2->length; j++) {
        S->ch[i] = S2->ch[j];
        S->length++;
        i++;
    }
    return OK;
}
void StringLength(String *S)//求字符串的长度
{
    printf("所求字符串的长度是：%d\n", S->length);
}
Status SubString(String *S, String *Sub, int pos, int len)//求字符串的子串
{
    int i;
    
    if(pos < 0 || pos > S->length)//起始位置小于零或大于字符串的长度
    {
        return ERROR;
    }
    else if( pos + len > S->length)//字符串从pos开始到结束没有len位
    {
        printf("长度太长\n");
        return ERROR;
    }
    else
    {
        printf("子串生成成功，sub=");
        for(i = pos - 1; i <= pos + len - 2; i++)
        {
            printf("%c", S->ch[i]);
        }
        printf("\n");
    }
    
    return OK;
}
Status Index(String *S, int pos, String *T)
{
    int i, j;
    int tag = 0;
    int temp = 0;
    
    if (pos < 0 || pos >= S->length) {
        return ERROR;
    }else{
        i = pos - 1;
        j = 0;
        while(i < S->length && j < T->length)
        {
            if (S->ch[i] == T->ch[j]) {
                if (j == 0) {
                    tag = i;
                }
                i++;
                j++;
            }
            else{
                if(j != 0)
                {
                    i = tag + 1;
                }
                else{
                    i = i + 1;
                }
                temp = i;
                j = 0;
            }
        }
        if(j == T->length)
        {
            printf("success\n");
            printf("position:%d\n", temp + 1);
        }
        else{
            printf("fail\n");
        }
    }
    return OK;
}
