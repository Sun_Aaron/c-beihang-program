//
//  test.c
//  实现一些小的功能算法
//
//  Created by zhuoooo on 2023/3/26.
//  Copyright © 2022年 zhuoooo. All rights reserved.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    int n, i, k, m=0;
    for (n = 2; n <= 200; n++) {
        k = sqrt(n);
        for (i = 2; i <= k; i++) {
            if (n%i == 0) {
                break;
            }
        }
        if (i >= k+1) {
            printf("%d  ", n);
            m = m + 1;
        }
        if (m % 10 == 0) {
            printf("\n");
        }
    }

    printf("\n");


    return 0;
}