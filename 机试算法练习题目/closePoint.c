//
//  main.c
//  ClosePoint-最近路程算法求解
//
//  Created by zhuoooo on 2022/5/10.
//  book P49
//  Copyright © 2022年 zhuoooo. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#define Max 100

int ClosestPoints(int x[], int y[], int n);//最近对算法求解


int main(int argc, const char * argv[]) {
    int n, i;
    int x[Max];
    int y[Max];
    printf("Input n: ");
    scanf("%d", &n);
    printf("Input x: \n");
    for(i = 0; i < n; i++)
    {
        scanf("%d", &x[i]);
    }
    printf("Input y: \n");
    for(i = 0; i < n; i++)
    {
        scanf("%d", &y[i]);
    }
    
    int mindist = ClosestPoints(x, y, n);
    printf("mindist = %d\n", mindist);
    
    
    return 0;
}


int ClosestPoints(int x[], int y[], int n)
{
    int index1, index2, d, i, j;   //index1,index2 记录最近点对的下标，ｄ表示最近距离
    int mindist = 1000;    //最近距离
    for(i = 0; i < n-1; i++)
    {
        for(j = i+1; j < n; j++)
        {
            d = (x[i] - x[j])*(x[i] - x[j]) + (y[i]-y[j])*(y[i]-y[j]);
            if(d < mindist)
            {
                mindist = d;
                index1 = i;
                index2 = j;
            }
        }
    }
    printf("最近点对的下标为: %d %d\n", index1+1, index2+1);
    return mindist;
}
